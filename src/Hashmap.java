import com.sun.org.apache.bcel.internal.generic.ATHROW;
import jdk.internal.org.objectweb.asm.tree.analysis.Value;

import java.util.Arrays;

public class Hashmap {

    private static int SIZE = 16;

    private Entry table[] = new Entry[SIZE];

    class Entry {
        String key;
        String value;
        Entry next;

        Entry(String k, String v) {
            key = k;
            value = v;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getKey() {
            return key;
        }
    }

    private int increaseHashSize() {
        return table.length * 2;
    }


    @Override
    public int hashCode() {
        return Arrays.hashCode(table);
    }

    public void put(String k, String v) {
        int hash = k.hashCode() % SIZE;
        table[hash] = new Entry(k, v);
    }

    public Entry get(String key) {
        int hash = key.hashCode() % SIZE;
        return table[hash];
    }

    private void resize() {
        int newSize = increaseHashSize();
        Entry[] newTable = new Entry[newSize];
        for (int i = 0; i < table.length; i++)
            newTable[i] = table[i];
        table = newTable;
    }

    public void remove(String key) {
        int hash = key.hashCode() % SIZE;
        if (key.equals(this.table[hash].key)) {
            System.out.println(table[hash].getValue() + " a été supprimé avec succès");
            table[hash] = null;
            resize();
            }
            else{
            System.out.println("L'element que vous voulez supprimer ne figure pas sur la liste");}
    }

        public void print () {
            for (int i = 0; i < table.length; i++) {
                if (table[i] != null)
                    System.out.println("Key" + i + " value " + table[i].getValue());
            }
        }
    }

