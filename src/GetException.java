public class GetException extends Exception {

    public GetException (String Message){
        super(Message);
    }
}
